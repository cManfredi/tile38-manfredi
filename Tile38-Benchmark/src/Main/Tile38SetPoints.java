package Main;

import java.util.ArrayList;
import java.util.Random;

import cmdLineParser.CommandLineValues;
import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import io.lettuce.core.codec.StringCodec;
import io.lettuce.core.output.StatusOutput;
import io.lettuce.core.protocol.CommandArgs;
import io.lettuce.core.protocol.CommandType;
import io.lettuce.core.protocol.ProtocolKeyword;

public class Tile38SetPoints {

	public static void main(String[] args) {
		Random rand = new Random();
		RedisClient client = RedisClient.create("redis://localhost:9851");
		StatefulRedisConnection<String, String> connection = client.connect();
		RedisCommands<String, String> sync = connection.sync();
		
		StringCodec codec = StringCodec.UTF8;
		
		for(int i = 0; i < 100000 ; i++){
			float lat = 33.1f + (rand.nextFloat() * (rand.nextFloat() < 0.5 ? -1 : 1));
			float lng = -112.4f + (rand.nextFloat() * (rand.nextFloat() < 0.5 ? -1 : 1));
			@SuppressWarnings("rawtypes")
			CommandArgs cmdArgs = new CommandArgs<>(codec);
			cmdArgs.add("key:bench");
			cmdArgs.add("id:"+i);
			cmdArgs.add("POINT");
			cmdArgs.add(String.format(java.util.Locale.US,"%.4f", lat));
			cmdArgs.add(String.format(java.util.Locale.US,"%.4f", lng));
			// Setting di punti random
			@SuppressWarnings("unchecked")
			String result = sync.dispatch(CommandType.SET, new StatusOutput<>(codec), cmdArgs);
		}
	}
	
	public static void log(String msg){
		System.out.println("[LOG] " + msg);
	}

}
