package cmdLineParser;

import java.util.Locale;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Localizable;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.ParserProperties;

public class CommandLineValues {
	@Option(name = "-m", aliases = { "--mode" }, required = false,
            usage = "Mode of execution: single (0) or cluster (1). Default: single")
	private int mode;
	@Option(name = "-c", aliases = { "--clients" }, required = false,
            usage = "Number of clients making the requests. Default: 50")
	private int clients;
	@Option(name = "-n", aliases = { "--requests" }, required = false,
            usage = "Number of total requests. Default: 100000")
	private int requests;
	
	@SuppressWarnings("deprecation")
	public CommandLineValues(String[] args) {
		ParserProperties props = ParserProperties.defaults();
		props.withUsageWidth(80);
        CmdLineParser parser = new CmdLineParser(this, props);
        try {
            parser.parseArgument(args);

            if (!(getMode() == 0 || getMode() == 1)) {
                throw new CmdLineException(parser,
                        "--mode has to be 0 or 1.");
            }
            
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            parser.printUsage(System.err);
        }
    }
	
	public int getMode(){
		return this.mode;
	}
	
	public int getClients(){
		return this.clients;
	}
	
	public int getRequests(){
		return this.requests;
	}

}
