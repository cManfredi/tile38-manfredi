package Main;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import io.lettuce.core.api.sync.RedisCommands;
import io.lettuce.core.codec.StringCodec;
import io.lettuce.core.output.NestedMultiOutput;
import io.lettuce.core.output.StatusOutput;
import io.lettuce.core.protocol.CommandArgs;
import io.lettuce.core.protocol.ProtocolKeyword;

public class ClientWorker extends Thread {
	
	private ProtocolKeyword cmd;
	private ArrayList<String> args;
	private ArrayList<RedisCommands<String, String>> syncAPIs;
	private int nrequests, pipeline;
	private Random rand;
	private List<Long> responseTimes;
	
	
	public ClientWorker(
			ArrayList<RedisCommands<String, String>> syncAPIs, 
			ProtocolKeyword cmd, 
			ArrayList<String> args, 
			List<Long> responseTimes, 
			int nrequests, 
			int pipeline) {
		this.syncAPIs = syncAPIs;
		this.cmd = cmd;
		this.args = args;
		this.nrequests = nrequests;
		this.pipeline = pipeline;
		this.responseTimes = responseTimes;
		this.rand = new Random();
	}
	
	@Override
	public void run() {
		StringCodec codec = StringCodec.UTF8;
		//Build CommandArgs
		@SuppressWarnings("rawtypes")
		CommandArgs cmdArgs = new CommandArgs<>(codec);
		for(String arg : this.args){
			cmdArgs.add(arg);
		}
		//Decide the client to use
		int size = this.syncAPIs.size();
		//Send commands
		if(size == 1){
			RedisCommands<String, String> sync = this.syncAPIs.get(0);
			for(int i = 0; i < nrequests; i++){
				long start = System.currentTimeMillis();
				if(this.cmd != CustomCommands.NEARBY){
					@SuppressWarnings("unchecked")
					String result = sync.dispatch(this.cmd, new StatusOutput<>(codec), cmdArgs);
				} else {
					@SuppressWarnings("unchecked")
					List<Object> result = sync.dispatch(this.cmd, new NestedMultiOutput<>(codec), cmdArgs);
				}
				long elapsed = System.currentTimeMillis() - start;
				this.responseTimes.add(elapsed);		
			}
		} else {
			for(int i = 0; i < this.nrequests; i++){
				long start = System.currentTimeMillis();
				if(this.cmd != CustomCommands.NEARBY){
					@SuppressWarnings("unchecked")
					String result = this.syncAPIs.get(this.rand.nextInt(size)).dispatch(this.cmd, new StatusOutput<>(codec), cmdArgs);
				} else {
					@SuppressWarnings("unchecked")
					List<Object> result = this.syncAPIs.get(this.rand.nextInt(size)).dispatch(this.cmd, new NestedMultiOutput<>(codec), cmdArgs);
				}
				long elapsed = System.currentTimeMillis() - start;
				this.responseTimes.add(elapsed);
			}
		}
	}

}
