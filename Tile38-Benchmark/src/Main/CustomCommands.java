package Main;

import io.lettuce.core.protocol.ProtocolKeyword;

public enum CustomCommands implements ProtocolKeyword {
	
    NEARBY;

    private final byte name[];

    CustomCommands() {
        // cache the bytes for the command name. Reduces memory and cpu pressure when using commands.
        name = name().getBytes();
    }

    @Override
    public byte[] getBytes() {
        return name;
    }
}
