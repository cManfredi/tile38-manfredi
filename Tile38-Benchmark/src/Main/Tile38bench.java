package Main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cmdLineParser.CommandLineValues;
import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import io.lettuce.core.codec.StringCodec;
import io.lettuce.core.output.CommandOutput;
import io.lettuce.core.output.StatusOutput;
import io.lettuce.core.protocol.CommandArgs;
import io.lettuce.core.protocol.CommandType;
import io.lettuce.core.protocol.ProtocolKeyword;
import io.lettuce.core.protocol.RedisCommand;
import io.netty.buffer.ByteBuf;

public class Tile38bench {

	public static void main(String[] args) {
		List<Long> responseTimes = Collections.synchronizedList(new ArrayList<Long>());
		
		int clients = Runtime.getRuntime().availableProcessors() - 1;
		int requests = 100000;
		int mode = 1; //0 : single - 1: cluster
		log("Using " + clients + " clients.");
		log("Sending " + requests + " requests.");
		log("Mode: " + (mode == 0 ? "Single" : "Cluster") +".");
		
		//Read args
		CommandLineValues values = new CommandLineValues(args);
	
		//Create connections
		ArrayList<RedisCommands<String, String>> syncAPIs = new ArrayList<>();
		if(mode == 0){
			RedisClient client = RedisClient.create("redis://localhost:9851");
			StatefulRedisConnection<String, String> connection = client.connect();
			syncAPIs.add(connection.sync());
		} else {
			RedisClient client1 = RedisClient.create("redis://localhost:9851");
			StatefulRedisConnection<String, String> conn1 = client1.connect();
			syncAPIs.add(conn1.sync());
			RedisClient client2 = RedisClient.create("redis://localhost:9852");
			StatefulRedisConnection<String, String> conn2 = client2.connect();
			syncAPIs.add(conn2.sync());
		}
		
		if(values.getRequests() != 0){
			requests = values.getRequests();
		}
		int reqPerClient = requests / clients;
		int reqLeft = requests % clients;
		
		ArrayList<ClientWorker> workers = new ArrayList<ClientWorker>();
		ArrayList<String> cmdArgs = new ArrayList<>();
		
//		PING TEST
		workers.clear();
		for(int i = 0; i < clients; i++){
			workers.add(new ClientWorker(
					syncAPIs, 
					CommandType.PING, 
					new ArrayList<String>(), 
					responseTimes,
					i == clients-1 ? reqPerClient+reqLeft : reqPerClient, 
					0)
			);
		}
		log("Starting PING test...");
		
//		executeTest(workers, requests, responseTimes);
		
		//GET Test
		workers.clear();
		cmdArgs.clear();
		cmdArgs.add("key1");
		cmdArgs.add("obj1");
		for(int i = 0; i < clients; i++){
			workers.add(new ClientWorker(
					syncAPIs, 
					CommandType.GET, 
					cmdArgs,
					responseTimes,
					i == clients-1 ? reqPerClient+reqLeft : reqPerClient, 
					0)
			);
		}
		log("Starting GET test...");
		
//		executeTest(workers, requests, responseTimes);
		
		//NEARBY
		workers.clear();
		cmdArgs.clear();
		cmdArgs.add("key:bench");
		cmdArgs.add("POINT");
		cmdArgs.add("33.1");
		cmdArgs.add("-112.4");
		cmdArgs.add("10000");
		for(int i = 0; i < clients; i++){
			workers.add(new ClientWorker(
					syncAPIs, 
					CustomCommands.NEARBY, 
					cmdArgs, 
					responseTimes,
					i == clients-1 ? reqPerClient+reqLeft : reqPerClient, 
					0)
			);
		}
		log("Starting NEARBY test...");

		executeTest(workers, requests, responseTimes);
		
		for(RedisCommands<String, String> api : syncAPIs){
			api.getStatefulConnection().close();
		}
		
	}
	
	public static void executeTest(
			ArrayList<ClientWorker> workers, 
			int requests,
			List<Long> responseTimes){
		responseTimes.clear();
		long start = System.currentTimeMillis();
		//Start workers
		for(ClientWorker worker: workers){
			worker.start();
		}
		for(ClientWorker worker: workers){
			try {
				worker.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		//Sync and calc time elapsed
		long elapsed = System.currentTimeMillis() - start;
		
		//Stampa dei risultati
		log("Test complete. Gathering results...");
		log(requests + " requests executed in " + elapsed + " ms ("+(int)((float)requests/elapsed*1000)+" requests per second)");
		int remaining = requests;
		float lastPercentage = 0;
		long limit = 0;
		while(lastPercentage != 100){
			float underThreshold = 0;
			for(long resTime : responseTimes){
				if(resTime <= limit){
					underThreshold++;
				}
			}
			float percentage = round(underThreshold / remaining * 100, 2);
			if(percentage != lastPercentage){
				// Stampa il numero di richieste eseguite
				log(percentage + "% requests executed in less then " + limit+ " ms");
				lastPercentage = percentage;
			}
			limit++;
		}
		log("------------------- NEXT TEST -------------------------");
	}
	
	public static void log(String msg){
		System.out.println("[LOG] " + msg);
	}
	
	public static float round(float number, int scale) {
	    int pow = 10;
	    for (int i = 1; i < scale; i++)
	        pow *= 10;
	    float tmp = number * pow;
	    return ( (float) ( (int) ((tmp - (int) tmp) >= 0.5f ? tmp + 1 : tmp) ) ) / pow;
	}
	
}
